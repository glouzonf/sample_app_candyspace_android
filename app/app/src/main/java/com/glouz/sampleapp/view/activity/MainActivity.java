package com.glouz.sampleapp.view.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.glouz.sampleapp.R;
import com.glouz.sampleapp.commons.utils.VisualUtils;
import com.glouz.sampleapp.view.fragment.SamplesListFragment;
import com.glouz.sampleapp.viewmodel.mainactivity.MainActivityViewInterface;
import com.glouz.sampleapp.viewmodel.mainactivity.MainActivityViewModel;

import butterknife.InjectView;
import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Ideally we would have diferent layouts for phone and tablets.
 * We would show the list and details of a selection in different activities.
 * We would have 2 fragments for list and details and would rearrange them for phone or tablet.
 * <p>
 * We would probably use the collapsing toolbar for a nice parallax effect on the detail page for using the cat images.
 */
public class MainActivity extends BaseViewModelActivity<MainActivityViewInterface, MainActivityViewModel> implements MainActivityViewInterface {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    private SweetAlertDialog loadingDialog;

    private SamplesListFragment samplesListFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            showFragment(SamplesListFragment.TAG);
        }

    }

    @Override
    void setContentView() {
        setContentView(R.layout.activity_main);
    }

    @Override
    void initialiseObjects() {

    }

    @Override
    void initialiseViews() {
    }

    @Override
    void updateViews() {
    }

    @Override
    void updateData() {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.list_menu:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public Class<MainActivityViewModel> getViewModelClass() {
        return MainActivityViewModel.class;
    }

    @Override
    public void showLoading(boolean show) {

        //hide keyboard if visible
        VisualUtils.hideKeyboard(mToolbar);

        if (loadingDialog == null) {
            loadingDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
            loadingDialog.setTitleText(getString(R.string.loading));
            loadingDialog.setCancelable(false);
        }

        if (loadingDialog != null && loadingDialog.isShowing()) {
            if (show) {
                loadingDialog.show();
            } else {
                loadingDialog.cancel();
            }
        }
    }

    @Override
    public void showError() {

    }

    public void showFragment(String fragmentTag) {

        showLoading(false);

        if (fragmentTag != null && fragmentTag.equals(SamplesListFragment.TAG)) {
            if (getSupportFragmentManager().findFragmentByTag(SamplesListFragment.TAG) == null) {
                samplesListFragment = SamplesListFragment.newInstance();

                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.commit();
            }

            fragmentTransaction.replace(R.id.main_container, samplesListFragment, SamplesListFragment.TAG);

        }
    }

}
