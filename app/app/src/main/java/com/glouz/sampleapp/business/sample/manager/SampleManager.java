package com.glouz.sampleapp.business.sample.manager;


import com.glouz.sampleapp.model.api.webapi.SampleWebapi;
import com.glouz.sampleapp.model.models.sample.Sample;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by glouzonf on 28/09/2015.
 */
public class SampleManager implements SampleManagerInterface {

    private SampleWebapi sampleWebapi;

    public SampleManager() {
        sampleWebapi = new SampleWebapi();
    }

    @Override
    public void getSamples(Subscriber subscriber) {
        Observable<Sample.SamplesList> getCats = sampleWebapi.getSamples();
        getCats.observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribe(subscriber);
    }

}
