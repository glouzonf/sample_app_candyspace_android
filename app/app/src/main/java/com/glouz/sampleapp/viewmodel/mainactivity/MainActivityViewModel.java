package com.glouz.sampleapp.viewmodel.mainactivity;

import android.support.annotation.NonNull;


import eu.inloop.viewmodel.AbstractViewModel;

/**
 * Created by glouzonf on 30/07/2015.
 */
public class MainActivityViewModel extends AbstractViewModel<com.glouz.sampleapp.viewmodel.mainactivity.MainActivityViewInterface> {

    public static final String TAG = MainActivityViewModel.class.getSimpleName();

    @Override
    public void bindView(@NonNull com.glouz.sampleapp.viewmodel.mainactivity.MainActivityViewInterface view) {
        super.bindView(view);


    }

}
