package com.glouz.sampleapp.view.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.glouz.sampleapp.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.Optional;
import eu.inloop.viewmodel.AbstractViewModel;
import eu.inloop.viewmodel.IView;
import eu.inloop.viewmodel.base.ViewModelBaseActivity;

public abstract class BaseViewModelActivity<T extends IView, S extends AbstractViewModel<T>> extends ViewModelBaseActivity<T, S> {

    public static final String TAG = BaseViewModelActivity.class.getSimpleName();

    protected FragmentManager fragmentManager = getSupportFragmentManager();
    protected FragmentTransaction fragmentTransaction;

    private ActionBarDrawerToggle mActionBarDrawerToggle;

    //toolbar
    @Optional
    @InjectView(R.id.toolbar)
    public Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setModelView((T) this);

        setContentView();
        ButterKnife.inject(this);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        initialiseObjects();
        initialiseViews();

    }

    abstract void setContentView();

    abstract void initialiseObjects();

    //add listeners or callbacks if necessary
    abstract void initialiseViews();

    abstract void updateViews();

    abstract void updateData();

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public ActionBarDrawerToggle getActionBarDrawerToggle() {
        return mActionBarDrawerToggle;
    }

}
