package com.glouz.sampleapp.model.models;

import android.util.Log;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.glouz.sampleapp.R;
import com.glouz.sampleapp.SampleAppApplication;
import com.glouz.sampleapp.commons.utils.ModelUtils;

import java.io.IOException;

/**
 * @author glouzonf
 */
@JsonIgnoreProperties(ignoreUnknown=true)
public class BaseModel extends Object {

    @Override
    public String toString() {
        try {
            return ModelUtils.getInstance().getObjectMapper().writeValueAsString(this);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(getClass().getSimpleName(), SampleAppApplication.getInstance().getString(R.string.error_deserializing_model));
        }

        return super.toString();
    }
}
