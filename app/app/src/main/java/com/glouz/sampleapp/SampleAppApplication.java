package com.glouz.sampleapp;

import android.app.Application;

/**
 * @author glouzonf
 */
public class SampleAppApplication extends Application {

    private static SampleAppApplication sApplication;

    public static SampleAppApplication getInstance() {
        return sApplication;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sApplication = this;

    }
}