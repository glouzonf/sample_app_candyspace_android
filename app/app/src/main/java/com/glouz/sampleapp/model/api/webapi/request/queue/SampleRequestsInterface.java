package com.glouz.sampleapp.model.api.webapi.request.queue;

import com.glouz.sampleapp.model.models.sample.Sample;

import retrofit.http.GET;
import rx.Observable;

/**
 * Created by glouzonf on 12/05/2015.
 */
public interface SampleRequestsInterface {

    @GET("/57ee2ca8260000f80e1110fa")
    Observable<Sample.SamplesList> getSample();
}
