package com.glouz.sampleapp.viewmodel.mainactivity;


import eu.inloop.viewmodel.IView;

/**
 * Created by glouzonf on 30/07/2015.
 */
public interface MainActivityViewInterface extends IView {
    void showLoading(boolean show);
    void showFragment(String fragmentTag);
    void showError();
}
