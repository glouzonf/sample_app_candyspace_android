package com.glouz.sampleapp.model.api.webapi;

import com.glouz.sampleapp.R;
import com.glouz.sampleapp.SampleAppApplication;
import com.glouz.sampleapp.model.api.webapi.request.queue.SampleRequestsInterface;
import com.glouz.sampleapp.model.models.sample.Sample;

import retrofit.RestAdapter;
import rx.Observable;

/**
 * Created by glouzaille on 06/05/2015.
 */
public class SampleWebapi extends BaseWebapi {

    public Observable<Sample.SamplesList> getSamples() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(SampleAppApplication.getInstance().getString(R.string.api_endpoint)).build();

        SampleRequestsInterface service = restAdapter.create(SampleRequestsInterface.class);

        return service.getSample();
    }
}
