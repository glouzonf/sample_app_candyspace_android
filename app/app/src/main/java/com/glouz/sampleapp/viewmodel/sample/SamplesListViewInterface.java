package com.glouz.sampleapp.viewmodel.sample;


import com.glouz.sampleapp.model.models.sample.Sample;

import eu.inloop.viewmodel.IView;

/**
 * Created by glouzonf on 30/07/2015.
 */
public interface SamplesListViewInterface extends IView {
    void showLoading(boolean show);
    void showError();
    void updateRecyclerView(Sample.SamplesList samplesList);
}
