package com.glouz.sampleapp.viewmodel.sample;

import android.support.annotation.NonNull;

import com.glouz.sampleapp.business.sample.manager.SampleManager;
import com.glouz.sampleapp.model.models.sample.Sample;

import eu.inloop.viewmodel.AbstractViewModel;
import rx.Subscriber;

/**
 * Created by glouzonf on 30/07/2015.
 */
public class SamplesListFragmentViewModel extends AbstractViewModel<SamplesListViewInterface> {

    public static final String TAG = SamplesListFragmentViewModel.class.getSimpleName();

    private SampleManager sampleManager;
    private Subscriber sampleSubscriber;
    private Sample.SamplesList samplesList;
    private boolean gettingSamples;

    @Override
    public void bindView(@NonNull SamplesListViewInterface view) {
        super.bindView(view);
    }


    public void getSamples() {
        if (samplesList == null && !gettingSamples) {
            gettingSamples = true;
            sampleSubscriber = new Subscriber<Sample.SamplesList>() {
                @Override
                public void onCompleted() {

                }

                @Override
                public void onError(Throwable e) {
                    //notify view of error and show appropriate message
                }

                @Override
                public void onNext(Sample.SamplesList resultSamplesList) {
                    gettingSamples = false;
                    samplesList = resultSamplesList;
                    getView().updateRecyclerView(samplesList);
                }
            };

            if (sampleManager == null) {
                sampleManager = new SampleManager();
            }
            sampleManager.getSamples(sampleSubscriber);
        } else if (samplesList != null && !gettingSamples) {
            getView().updateRecyclerView(samplesList);
        }
    }

    @Override
    public void onModelRemoved() {
        super.onModelRemoved();
        //use this to cancel any planned requests
    }

}
