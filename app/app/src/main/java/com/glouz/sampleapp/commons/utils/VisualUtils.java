package com.glouz.sampleapp.commons.utils;

import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.glouz.sampleapp.SampleAppApplication;


/**
 * Created by glouzonf on 30/09/2015.
 */
public class VisualUtils extends BaseUtils{

    private static void hideKeyboard(Context context, View v) {
        InputMethodManager imm = ((InputMethodManager) context
                .getSystemService(Context.INPUT_METHOD_SERVICE));
        if (imm != null && v != null) {
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public static void hideKeyboard(View v) {
        hideKeyboard(SampleAppApplication.getInstance(), v);
    }


}
