package com.glouz.sampleapp.model.adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.glouz.sampleapp.R;
import com.glouz.sampleapp.model.models.sample.Sample;
import com.squareup.picasso.Picasso;

import java.util.List;


public class SampleAdapter extends RecyclerView.Adapter<SampleAdapter.SimpleViewHolder> {
    private static final int VIEW_TYPE_DEFAULT = 1;

    private Context context;
    private List<Sample> sampleList;

    private int width_resize = 200;
    private int eight_resize = 100;

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final CardView card;
        public final TextView sampleTitle;
        public final ImageView sampleImage;
        public final TextView sampleText;

        public SimpleViewHolder(View view) {
            super(view);
            card = (CardView) view.findViewById(R.id.card);
            sampleTitle = (TextView) view.findViewById(R.id.title_sample);
            sampleImage = (ImageView) view.findViewById(R.id.image_sample);
            sampleText = (TextView) view.findViewById(R.id.text_sample);

        }
    }


    public SampleAdapter(Context context, List<Sample> sampleList) {
        this.context = context;
        this.sampleList = sampleList;
    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_DEFAULT;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (viewType == VIEW_TYPE_DEFAULT) {
            view = LayoutInflater.from(context).inflate(R.layout.item_sample_list, parent, false);
            return new SimpleViewHolder(view);
        }

        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {


        if (sampleList != null && sampleList.size() > 0) {

            if (sampleList.get(position).image != null) {
                Picasso.with(context).load(sampleList.get(position).image).resize(width_resize, eight_resize).into(holder.sampleImage);
            }

            if (sampleList.get(position) != null) {
                if (sampleList.get(position).title != null) {
                    holder.sampleTitle.setText(sampleList.get(position).title);
                }

                if (sampleList.get(position).description != null) {
                    holder.sampleText.setText(sampleList.get(position).description);
                }
            }
        }
    }

    public Sample getItem(int position) {
        return (sampleList != null && sampleList.size() > 0) ? sampleList.get(position) : null;
    }

    @Override
    public int getItemCount() {

        if (sampleList != null) {
            return sampleList.size();
        }

        return 0;
    }

}
