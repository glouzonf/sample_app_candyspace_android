package com.glouz.sampleapp.business.sample.manager;

import rx.Subscriber;

/**
 * Created by glouzonf on 28/09/2015.
 */
public interface SampleManagerInterface {

    void getSamples(Subscriber subscriber);

}
