package com.glouz.sampleapp.view.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glouz.sampleapp.R;

import butterknife.ButterKnife;
import eu.inloop.viewmodel.AbstractViewModel;
import eu.inloop.viewmodel.IView;
import eu.inloop.viewmodel.base.ViewModelBaseFragment;

//import net.glouz.myapp.model.api.service.RequestServiceMockRESTapiSampleApp;

/**
 * Created by glouzonf on 05/05/2015.
 * <p>
 * Base fragment that inflate the root layout of the current fragment and initialize the appropriate
 * related views.
 * <p>
 * The layout id has to be set in the constructor of the extending fragment
 * and the views declared using the butterknife annotations
 * (i.e @InjectView(R.id.button1) Button button1;)
 */
public abstract class BaseFragmentViewModel<T extends IView, S extends AbstractViewModel<T>> extends ViewModelBaseFragment<T, S> {

    protected int layoutId;
    protected View rootView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (layoutId != 0) {
            // Inflate the layout for this fragment
            rootView = inflateRootView(layoutId, inflater, container, savedInstanceState);
        } else {
            throw new IllegalArgumentException(getString(R.string.fragment_layout_missing_exception, this.getClass().getSimpleName()));
        }

        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.inject(this, rootView);

        setModelView((T) this);

        initialiseObjects();
        initialiseViews();
        updateData();
    }

    abstract void initialiseObjects();

    //add listeners or callbacks if necessary
    abstract void initialiseViews();

    abstract void updateViews();

    abstract void updateData();

    private View inflateRootView(int layoutId, LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(layoutId, container, false);
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

}
