package com.glouz.sampleapp.view.fragment;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glouz.sampleapp.R;
import com.glouz.sampleapp.model.adapter.SampleAdapter;
import com.glouz.sampleapp.model.models.sample.Sample;
import com.glouz.sampleapp.view.ItemClickSupport;
import com.glouz.sampleapp.viewmodel.sample.SamplesListFragmentViewModel;
import com.glouz.sampleapp.viewmodel.sample.SamplesListViewInterface;

import butterknife.InjectView;

/**
 * Created by glouzonf.
 * <p>
 * The view model handle the model during the lifecycle of the fragment/activity
 * it's specially useful for restoring state on screen rotation.
 * the view model is acting like a controller providing separation between the
 */
public class SamplesListFragment extends BaseFragmentViewModel<SamplesListViewInterface, SamplesListFragmentViewModel> implements SamplesListViewInterface {

    public static final String TAG = SamplesListFragment.class.getSimpleName();

    @InjectView(R.id.list_sample)
    public RecyclerView listSamplesrecyclerView;

    private LinearLayoutManager listLayoutManager;


    private SampleAdapter samplesAdapter;

    private Sample.SamplesList samplesList = new Sample.SamplesList();

    public SamplesListFragment() {
    }

    public static SamplesListFragment newInstance() {
        SamplesListFragment catsListFragment = new SamplesListFragment();
        Bundle args = new Bundle();
        catsListFragment.setArguments(args);
        return catsListFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setLayoutId(R.layout.sample_layout);
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    void initialiseObjects() {
    }

    @Override
    public Class<SamplesListFragmentViewModel> getViewModelClass() {
        return SamplesListFragmentViewModel.class;
    }

    @Override
    void initialiseViews() {
        initRecyclerView();
    }

    @Override
    void updateViews() {
    }

    @Override
    void updateData() {
        getViewModel().getSamples();
    }


    @Override
    public void showLoading(boolean show) {
    }

    @Override
    public void showError() {
    }

    private void initRecyclerView() {


        samplesAdapter = new SampleAdapter(getActivity(), samplesList);

        listLayoutManager = new LinearLayoutManager(getContext(), OrientationHelper.VERTICAL, false);
        listSamplesrecyclerView.setLayoutManager(listLayoutManager);
        listSamplesrecyclerView.setItemAnimator(new DefaultItemAnimator());
        listSamplesrecyclerView.setAdapter(samplesAdapter);

        ItemClickSupport.addTo(listSamplesrecyclerView).setOnItemClickListener(
                new ItemClickSupport.OnItemClickListener() {
                    @Override
                    public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                        if (samplesList != null && samplesList.size() > position &&
                                samplesList.get(position) != null) {
                            //pass parcelable object to detail.
                            Log.e("clicked" + position, "" + samplesList.get(position).toString());
                        }
                    }
                }
        );
    }

    public void updateRecyclerView(Sample.SamplesList samplesList) {
        if (samplesAdapter != null) {
            this.samplesList.clear();
            this.samplesList.addAll(samplesList);
            samplesAdapter.notifyDataSetChanged();
        }
    }
}