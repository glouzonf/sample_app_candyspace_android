
package com.glouz.sampleapp.model.models.sample;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.glouz.sampleapp.model.models.BaseModel;

import java.util.ArrayList;
import java.util.List;

public class Sample extends BaseModel {

    @JsonProperty("id")
    public String id;
    @JsonProperty("description")
    public String description;
    @JsonProperty("date")
    public String date;
    @JsonProperty("tags")
    public List<String> tags = new ArrayList<>();
    @JsonProperty("title")
    public String title;
    @JsonProperty("image")
    public String image;

    public static class SamplesList extends ArrayList<Sample> {
    }

}
