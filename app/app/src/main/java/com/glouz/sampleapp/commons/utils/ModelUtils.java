package com.glouz.sampleapp.commons.utils;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.glouz.sampleapp.R;
import com.glouz.sampleapp.SampleAppApplication;

import java.io.IOException;

/**
 * Created by glouzonf on 27/05/2015.
 */
public class ModelUtils {

    private static ModelUtils modelUtils;
    private ObjectMapper mapper;

    public static ModelUtils getInstance() {
        if (modelUtils == null) {
            modelUtils = new ModelUtils();
        }

        return modelUtils;
    }

    public ObjectMapper getObjectMapper (){
        if (mapper == null){
            mapper = new ObjectMapper();
        }
        return mapper;
    }

    public String jsonToString (Object object){
        String value = null;
        try {
            value = ModelUtils.getInstance().getObjectMapper().writeValueAsString(object);
        } catch (IOException e) {
            e.printStackTrace();
            Log.e(getClass().getSimpleName(), SampleAppApplication.getInstance().getString(R.string.error_deserializing_model));
        }

        return value;
    }
}
